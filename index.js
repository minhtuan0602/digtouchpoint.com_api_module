var request = require('request');

const HOST = "http://app.digtouchpoint.com/api";

var digTouchPointApi = {
	accessToken: null
}

var init = function(accessToken) {
	digTouchPointApi.accessToken = accessToken;
}

var syncCustomers = function(customers, callback) {
	if (!digTouchPointApi.accessToken) {
		throw new Error("ACCESS_TOKEN IS REQUIRED");
	}

	request({
		method: "POST",
		url: HOST + "/customers/sync",
		headers: {
	    'Content-Type': 'application/json',
	    'token': digTouchPointApi.accessToken
	  },
	  body: JSON.stringify({
	  	customers: customers
	  })
	}, function(error, response, body){
		if(error) {
			return callback(error)
		}

	 	if (!error && response.statusCode == 200) {
	    var res = JSON.parse(body);
	    return callback(null, res);
	  }
	});
}

var syncOrders = function(orders, callback) {
	if (!digTouchPointApi.accessToken) {
		throw new Error("ACCESS_TOKEN IS REQUIRED");
	}

	request({
		method: "POST",
		url: HOST + "/orders/sync",
		headers: {
	    'Content-Type': 'application/json',
	    'token': digTouchPointApi.accessToken
	  },
	  body: JSON.stringify({
	  	orders: orders
	  })
	}, function(error, response, body){
		if(error) {
			return callback(error)
		}

	 	if (!error && response.statusCode == 200) {
	    var res = JSON.parse(body);
	    return callback(null, res);
	  }
	});
}

var syncOrderDetails = function(details, callback) {
	if (!digTouchPointApi.accessToken) {
		throw new Error("ACCESS_TOKEN IS REQUIRED");
	}

	request({
		method: "POST",
		url: HOST + "/order-details/sync",
		headers: {
	    'Content-Type': 'application/json',
	    'token': digTouchPointApi.accessToken
	  },
	  body: JSON.stringify({
	  	details: details
	  })
	}, function(error, response, body){
		if(error) {
			return callback(error)
		}

	 	if (!error && response.statusCode == 200) {
	    var res = JSON.parse(body);
	    return callback(null, res);
	  }
	});
}

module.exports = {
	init: init,
	syncCustomers: syncCustomers,
	syncOrders: syncOrders,
	syncOrderDetails: syncOrderDetails
}